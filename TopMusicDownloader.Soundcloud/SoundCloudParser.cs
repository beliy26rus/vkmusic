﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using HtmlAgilityPack;
using Newtonsoft.Json;
using TopMusicDownloader.Common;

namespace TopMusicDownloader.Soundcloud
{
    public class SoundCloudParser : IParser
    {
        public void Authorize(string token)
        {
        }

        public async Task<List<IAudio>> GetPopularAudios(string genre)
        {
            return await Task.Factory.StartNew(() => GePopularTrack(genre).Cast<IAudio>().ToList());
        }

        public bool IsAuthorized()
        {
            return true;
        }

        public List<string> Genres
        {
            get { return _genres.Select(item => item.Key).ToList(); }
        }


        private readonly Dictionary<string, string> _genres = new Dictionary<string, string>()
        {
            {"Drum & Bass", "drumbass"},
            {"Dubstep", "dubstep"},
            {"House","house" },
            {"Trance","trance" },
            {"Electronic","electronic" }
        };

        public bool NeedAuthorization { get; } = false;


        private const string ClientId = "02gUJC0hH2ct1EGOcYXQIzRFU91c72Ea";

        private HtmlDocument GetHtmlDocument(string genre)
        {
            var html = new HtmlDocument();
            var webclient = new WebClient();
            webclient.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");
            var result = webclient.DownloadString($"https://soundcloud.com/charts/top?genre={genre}");
            html.LoadHtml(result);
            return html;
        }

        private AudioSoundCloud GetAudioSoundCloud(string url)
        {
            var client = new WebClient();
            var result = client.DownloadString($"https://api.soundcloud.com/resolve.json?url={url}&client_id={ClientId}");
            AudioSoundCloud track = JsonConvert.DeserializeObject<AudioSoundCloud>(result);
            track.Url = $"{track.Url}?client_id={ClientId}";
            return track;
        }

        private List<AudioSoundCloud> GePopularTrack(string genre)
        {
            var document = GetHtmlDocument(_genres[genre]);
            var links =
                document.DocumentNode.SelectSingleNode("/html[1]/body[1]/div[1]/noscript[2]/section[1]/section[2]/ol[1]")
                    .ChildNodes.Where(item => item.Name == "li")
                    .Select(item => item.ChildNodes[1].ChildNodes[0].FirstChild.Attributes["href"].Value)
                    .Select(item => $"https://soundcloud.com{item}")
                    .ToList();
            var audios = links.Select(GetAudioSoundCloud).ToList();
            foreach (var audio in audios)
                audio.Genre = genre;
            return audios;
        }
    }
}