﻿using System;

namespace TopMusicDownloader.Common
{
    public interface IAudio
    {
        string Artist { get; set; }

        string Title { get; set; }

        string Url { get; set; }

        TimeSpan Time { get; }

        string Name { get;}

        string Genre { get; set; }

        string Prefix { get; }
    }
}