using System;
using Newtonsoft.Json;
using TopMusicDownloader.Common;
using TopVkMusic;

namespace TopMusicDownloader.VK
{
    [JsonObject]
    public class AudioVk : IAudio
    {
        [JsonProperty("artist")]
        public string Artist { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("duration")]
        public uint Duration { get; set; }

        [JsonProperty("url")]
        public string Url { get; set; }

        public string Name => $"{Artist} - {Title}";

        public string Genre { get; set; }

        public TimeSpan Time => new TimeSpan(0, 0, 0, (int) Duration);

        public string Prefix => "VK";

    }
}