﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace TopMusicDownloader.VK
{
    [JsonObject]
  
    public class AudioObject
    {
        [JsonProperty("response")]
        public List<AudioVk> Audio { get; set; }
    }
}
