﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using TopMusicDownloader.Common;

namespace TopMusicDownloader.VK
{
    public class VkPasrer : IParser
    {
        private const string Query = "https://api.vk.com/method/";

        private string _accessToken = string.Empty;

        public bool IsAuthorized()
        {
            return !string.IsNullOrWhiteSpace(_accessToken);
        }

        public List<string> Genres
        {
            get { return _genres.Select(item => item.Key).ToList(); }
        }

        public bool NeedAuthorization => true;

        public void Authorize(string accessToken)
        {
            _accessToken = accessToken;
        }

        private async Task<string> ApiGetPopular(int genre, int offset = 0, int count = 100, bool onlyEng=true)
        {
            return await Task.Factory.StartNew(() =>
            {
                if (string.IsNullOrWhiteSpace(_accessToken))
                    throw new Exception("Access Token is empty");
                var query =
                    $"{Query}audio.getPopular?only_eng={(onlyEng ? 1 : 0)}&genre_id={genre}&offset={offset}&count={count}&access_token={_accessToken}";
                var request = WebRequest.Create(query);
                var stream = request.GetResponse().GetResponseStream();
                if (stream == null) return null;
                var streamReader = new StreamReader(stream);
                return streamReader.ReadToEnd();
            });
        }

        private int GetGenreId(string genre)
        {
            return _genres[genre];
        }

        private readonly Dictionary<string, int> _genres = new Dictionary<string, int>()
        {
            {"Rock", 1},
            {"Pop", 2},
            {"RapHipHop", 3},
            {"EasyListening", 4},
            {"DanceHouse", 5},
            {"Instrumental", 6},
            {"Metal", 7},
            {"Alternative", 21},
            {"Dubstep", 8},
            {"JazzBlues", 9},
            {"DrumBass", 10},
            {"Trance", 11},
            {"Chanson", 12},
            {"Ethnic", 13},
            {"AcousticVocal", 14},
            {"Reggae", 15},
            {"Classical", 16},
            {"IndiePop", 17},
            {"Speech", 19},
            {"ElectropopDisco", 22},
            {"Other", 18}
        };

        public async Task<List<IAudio>> GetPopularAudios(string genre)
        {
            var jsonString = await ApiGetPopular(GetGenreId(genre));
            var x = (AudioObject) JsonConvert.DeserializeObject(jsonString, typeof (AudioObject));
            foreach (var audio in x.Audio)
                audio.Genre = genre;
            return x.Audio.Cast<IAudio>().ToList();
        }
    }
}