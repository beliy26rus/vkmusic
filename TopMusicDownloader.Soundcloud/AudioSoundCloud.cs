﻿using System;
using Newtonsoft.Json;
using TopMusicDownloader.Common;

namespace TopMusicDownloader.Soundcloud
{
    [JsonObject]
    public class AudioSoundCloud : IAudio
    {
        public string Artist { get; set; }

        public string Title { get; set; }

        [JsonProperty("stream_url")]
        public string Url { get; set; }

        public TimeSpan Time => new TimeSpan(0, 0, 0, 0, (int) Duration);

        public string Name => $"{Title}";

        public string Genre { get; set; }

        public uint Duration { get; set; }

        public string Prefix => "SC";
    }
}