﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Windows.Forms;

namespace TopVkMusic.Ui
{
    public partial class FormAuth : Form
    {
        public string AccessToken;

        public FormAuth()
        {
            InitializeComponent();
            webBrowser1.ScriptErrorsSuppressed = true;
        }

        private void FormAuth_Load(object sender, EventArgs e)
        {
            webBrowser1.Navigate(
              $"https://oauth.vk.com/authorize?client_id=5046059&redirect_uri=https://oauth.vk.com/blank.html&response_type=token&scope=audio&display=page&v=5.40");
            webBrowser1.DocumentCompleted += delegate (object o, WebBrowserDocumentCompletedEventArgs args)
            {
                var urlParams = HttpUtility.ParseQueryString(args.Url.Fragment.Substring(1));
                AccessToken = urlParams.Get("access_token");
                if (!string.IsNullOrWhiteSpace(AccessToken))
                    Close();
                else
                {
                    Opacity = 100;
                }
            };
        }
    }
}
