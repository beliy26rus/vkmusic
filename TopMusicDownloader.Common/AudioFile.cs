﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace TopMusicDownloader.Common
{
    public sealed class AudioFile : INotifyPropertyChanged
    {
        private int _number;

        public int Number
        {
            get { return _number; }
            set
            {
                _number = value; 
                OnPropertyChanged();
            }
        }


        private int _percent;
        private float _totalBytesToReceive;

        public AudioFile(IAudio audio, int number)
        {
            if (audio == null)
            {
                throw new ArgumentNullException(nameof(audio));
            }
            Name = $"{audio.Name}.mp3";
            Prefix = audio.Prefix;
            Url = audio.Url;
            Genre = audio.Genre;
            Number = number;
        }

        public string Prefix { get; }

        public string Name { get; set; }

        public string Genre { get; private set; }

        public float TotalBytesToReceive
        {
            get { return _totalBytesToReceive; }
            set
            {
                _totalBytesToReceive = value;
                OnPropertyChanged();
            }
        }
       

        public string Url { get; private set; }

        public int Percent
        {
            get { return _percent; }
            set
            {
                _percent = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;


        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}