﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace TopMusicDownloader.Common
{
    public interface IParser
    {
        void Authorize(string token);

        Task<List<IAudio>> GetPopularAudios(string genre);

        bool IsAuthorized();

        List<string> Genres { get;  }

        bool NeedAuthorization { get; }
    }
}
