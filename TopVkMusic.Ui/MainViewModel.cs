﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using TopMusicDownloader.Common;
using TopMusicDownloader.Soundcloud;
using TopMusicDownloader.Ui.Properties;
using TopMusicDownloader.VK;
using TopVkMusic.Ui;

namespace TopMusicDownloader.Ui
{
    public class MainViewModel : ViewModelBase
    {
        private IParser _parser;
        private IAudio _audio;
        private AudioFile _audioFile;
        private ObservableCollection<IAudio> _audios;
        private CancellationTokenSource _cancellationToken;

        private int _countAudios;

        private int _errorCount;

        private string _folder;
        private string _genre;
        private bool _isAutorized;

        private bool _isBusy;

        private bool _loading;

        private int _ready;


        public MainViewModel()
        {
            AudioFiles = new ObservableCollection<AudioFile>();
            Audios = new ObservableCollection<IAudio>();
            Audios.CollectionChanged += (sender, args) => { CountAudios = Audios.Count; };
            Folder = Settings.Default.Folder;
            CurrentParser = Parsers.First();
        }

        public int CountAudios
        {
            get { return _countAudios; }
            set
            {
                _countAudios = value;
                RaisePropertyChanged(nameof(CountAudios));
            }
        }

        public bool IsAutorized
        {
            get { return _isAutorized; }
            set
            {
                _isAutorized = value;
                RaisePropertyChanged(nameof(IsAutorized));
            }
        }

        public string Genre
        {
            get { return _genre; }
            set
            {
                _genre = value;
                RaisePropertyChanged(nameof(Genre));
            }
        }

        public IAudio Audio
        {
            get { return _audio; }
            set
            {
                _audio = value;
                RaisePropertyChanged(nameof(Audio));
            }
        }

        public ObservableCollection<IAudio> Audios
        {
            get { return _audios; }
            set
            {
                _audios = value;
                RaisePropertyChanged(nameof(Audios));
            }
        }

        public AudioFile AudioFile
        {
            get { return _audioFile; }
            set
            {
                _audioFile = value;
                RaisePropertyChanged(nameof(AudioFile));
            }
        }

        public string Folder
        {
            get { return _folder; }
            set
            {
                _folder = value;
                RaisePropertyChanged(nameof(Folder));
            }
        }

        public int Ready
        {
            get { return _ready; }
            set
            {
                _ready = value;
                RaisePropertyChanged(nameof(Ready));
            }
        }

        public int ErrorCount
        {
            get { return _errorCount; }
            set
            {
                _errorCount = value;
                RaisePropertyChanged(nameof(ErrorCount));
            }
        }

        public bool IsBusy
        {
            get { return _isBusy; }
            set
            {
                _isBusy = value;
                RaisePropertyChanged(nameof(IsBusy));
            }
        }

        public bool Loading
        {
            get { return _loading; }
            set
            {
                _loading = value;
                RaisePropertyChanged(nameof(Loading));
            }
        }

        private List<string> _genres;

        public List<string> Genres
        {
            get { return _genres; }
            set
            {
                _genres = value;
                RaisePropertyChanged(nameof(Genres));
            }
        }

        public List<string> Parsers { get; set; } = new List<string>()
        {
            "VK.com",
            "soundcloud.com"
        };

        private string _currentParser;

        public string CurrentParser
        {
            get { return _currentParser; }
            set
            {
                _currentParser = value;
                RaisePropertyChanged(nameof(CurrentParser));
                ChangeParser();
            }
        }

        private bool _needAuthorization;

        public bool NeedAuthorization
        {
            get { return _needAuthorization; }
            set
            {
                _needAuthorization = value;
                RaisePropertyChanged(nameof(NeedAuthorization));
            }
        }


        private void ChangeParser()
        {
            switch (CurrentParser)
            {
                case "VK.com":
                    _parser = new VkPasrer();
                    break;

                case "soundcloud.com":
                    _parser = new SoundCloudParser();
                    break;
            }
            Genres = _parser.Genres;
            Genre = Genres.First();
            NeedAuthorization = _parser.NeedAuthorization;
            IsAutorized = _parser.IsAuthorized();
        }

        public ObservableCollection<AudioFile> AudioFiles { get; set; }

        public RelayCommand AddRelayCommand => new RelayCommand(Add, () => Audio != null);

        public RelayCommand AddAllRelayCommand => new RelayCommand(AddAll, () => Audios != null && Audios.Any());

        public RelayCommand ChoisePathRelayCommand => new RelayCommand(ChoisePath);

        public RelayCommand LoadAudiosRelayCommand => new RelayCommand(
            async () =>
            {
                IsBusy = true;
                Audios = new ObservableCollection<IAudio>(await _parser.GetPopularAudios(Genre));
                IsBusy = false;
            }, () => IsAutorized);

        public RelayCommand AuthorizeRelayCommand => new RelayCommand(Authorize);

        public RelayCommand LoadRelayCommand
            =>
                new RelayCommand(Download,
                    () =>
                        AudioFiles != null && AudioFiles.Any() && IsAutorized && !string.IsNullOrEmpty(Folder) &&
                        !Loading);

        public RelayCommand RemoveRelayCommand
            => new RelayCommand(() => AudioFiles.Remove(AudioFile), () => AudioFile != null);

        public RelayCommand ClearRelayCommand => new RelayCommand(() => AudioFiles.Clear(), () => AudioFiles.Any());

        public RelayCommand StopRelayCommand
            =>
                new RelayCommand(() => _cancellationToken.Cancel(true),
                    () => _cancellationToken != null && !_cancellationToken.IsCancellationRequested);

        public RelayCommand OpenFolderRelayCommand
            => new RelayCommand(OpenFolder, () => !string.IsNullOrWhiteSpace(Folder));

        private void OpenFolder()
        {
            if (!Directory.Exists(Folder))
            {
                MessageBox.Show("Невозможно открыть папку", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            Process.Start(Folder);
        }

        private void ChoisePath()
        {
            var openFolderDialog = new FolderBrowserDialog();
            if (openFolderDialog.ShowDialog() != DialogResult.OK)
                return;
            Folder = openFolderDialog.SelectedPath;
            Settings.Default.Folder = Folder;
            Settings.Default.Save();
        }

        private void Authorize()
        {
            IsBusy = true;
            var form = new FormAuth();
            form.ShowDialog();
            _parser.Authorize(form.AccessToken);
            IsAutorized = _parser.IsAuthorized();
            IsBusy = false;
        }

        private async void Download()
        {
            _cancellationToken = new CancellationTokenSource();
            Loading = true;
            await Task.Factory.StartNew(async () =>
            {
                Ready = 0;
                ErrorCount = 0;
                AudioFiles.ToList().ForEach(item => item.Percent = 0);
                foreach (var audiofile in AudioFiles)
                {
                    if (_cancellationToken.IsCancellationRequested)
                    {
                        Loading = false;
                        _cancellationToken = new CancellationTokenSource();
                        return;
                    }

                    var path = Path.Combine(Folder, audiofile.Genre + "_" + audiofile.Prefix);
                    if (!Directory.Exists(path))
                        Directory.CreateDirectory(path);
                    var client = new WebClient();
                    client.DownloadProgressChanged +=
                        (o, eventArgs) =>
                        {
                            audiofile.Percent = eventArgs.ProgressPercentage;
                            audiofile.TotalBytesToReceive = eventArgs.TotalBytesToReceive;
                        };
                    try
                    {
                        await
                            client.DownloadFileTaskAsync(new Uri(audiofile.Url),
                                Path.Combine(path, RemoveIllegalCharacter(audiofile.Name)));
                        Ready++;
                    }
                    catch (Exception)
                    {
                        ErrorCount++;
                    }
                    Loading = false;
                }
            }, _cancellationToken.Token);
        }

        private string RemoveIllegalCharacter(string path)
        {
            string regexSearch = new string(Path.GetInvalidFileNameChars()) + new string(Path.GetInvalidPathChars());
            Regex r = new Regex($"[{Regex.Escape(regexSearch)}]");
            var correctName = r.Replace(path, "");
            return correctName;
        }

        private void Add()
        {
            var client = new WebClient();
            client.OpenRead(Audio.Url);
            var bytesTotal = Convert.ToInt64(client.ResponseHeaders["Content-Length"]);
            var number = AudioFiles.Count + 1;
            AudioFiles.Add(new AudioFile(Audio, number) {TotalBytesToReceive = bytesTotal,});
        }

        private void AddAll()
        {
            foreach (var audio in Audios.Where(audio => !AudioFiles.Select(auf => auf.Url).Contains(audio.Url)))
            {
                var number = AudioFiles.Count + 1;
                AudioFiles.Add(new AudioFile(audio, number));
            }
        }
    }
}